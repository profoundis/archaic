Date: 2013-03-22
Title: Artificial Intelligence
Tags: AI,Intelligence,Artificial Intelligence
Slug: artificial_intelligence
Author: Nitin Saxena
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/ai.jpg" width="50%">


**What’s “Artificial Intelligence” ?**

Artificial Intelligence is a broad discipline that promises to simulate numerous innate human skills such as automatic programming, case-based reasoning, neural networks, decision-making, expert
systems, natural language processing, pattern recognition and speech recognition etc.‘AI’ technologies bring more complex data-analysis features to existing applications.

There are many definitions that attempt to explain what Artificial Intelligence (AI) is.
‘AI’ is often termed as science that investigates knowledge and intelligence, possibly the intelligent
application of knowledge. Knowledge and Intelligence are as fundamental as the universe within
which they exist, it may turn out that they are more fundamental.

One of the aims of ‘AI’ is said to be the investigation of human cognition and ‘AI’ is part of Cognitive
Science. ‘AI’ is really an investigation into the creation of intelligence and that there is no reason for
the intelligence that is created to be exactly the same as human intelligence.

The potential applications of Artificial Intelligence are abundant. They stretch from the military for
autonomous control and target identification, to the entertainment industry for computer games
and robotic pets, to the big establishments dealing with huge amounts of information such as
hospitals, banks and insurances, we can also use ‘AI’ to predict customer behaviour and detect
trends.

**Need of Artificial Intelligence**

Enterprises that utilize ‘AI’ enhanced applications are expected to become more diverse, as the
needs for the ability to analyze data across multiple variables, fraud detection and customer
relationship management emerge as key business drivers to gain competitive advantage.

Artificial Intelligence is a branch of Science which deals with helping machines, finds solutions to
complex problems in a more human-like fashion. This generally involves borrowing characteristics
from human intelligence, and applying them as algorithms in a computer friendly way. A more or
less flexible or efficient approach can be taken depending on the requirements established, which
influences how artificial the intelligent behaviour appears.

‘AI’ is generally associated with Computer Science, but it has many important links with other fields
such as Maths, Psychology, Cognition, Biology and Philosophy, among many others. Our ability to
combine knowledge from all these fields will ultimately benefit our progress in the quest of creating
an intelligent artificial being.

**Role “Artificial Intelligence” in Business**

‘AI’ technologies bring more complex data-analysis features to existing applications.
Business applications utilize the specific technologies mentioned earlier to try and make better
sense of potentially enormous variability (for example, unknown patterns/relationships in sales data,
customer buying habits, and so on).

However, within the corporate world, ‘AI’ is widely used for complex problem-solving and decision-
support techniques in real-time business applications. The business applicability of ‘AI’ techniques is
spread across functions ranging from finance management to forecasting and product

Artificial Intelligence (AI) has been used in business applications since the early eighties. As with all
technologies, ‘AI’ initially generated much interest, but failed to live up to the hype. However, with
the advent of web-enabled infrastructure and rapid strides made by the ‘AI’ development
community, the application of ‘AI’ techniques in real-time business applications has picked up
substantially in the recent past.

Computers are fundamentally well suited to performing mechanical computations, using fixed
programmed rules. This allows artificial machines to perform simple monotonous tasks efficiently
and reliably, which humans are ill-suited to. For more complex problems, things get more difficult.
Unlike humans, computers have trouble understanding specific situations, and adapting to new
situations. Artificial Intelligence aims to improve machine behaviour in tackling such complex tasks.

Together with this, much of ‘AI’ research is allowing us to understand our intelligent behaviour.
Humans have an interesting approach to problem-solving, based on abstract thought, high-level
deliberative reasoning and pattern recognition. Artificial Intelligence can help us understand this
process by recreating it, then potentially enabling us to enhance it beyond our current capabilities.
