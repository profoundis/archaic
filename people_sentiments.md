Date: 2012-12-31
Title: The people factor in Sentiment Analytics
Tags: Business Intelligence, Business Analytics, Sentiment analytics, Klout, people map, social media
Slug: People_Sentiments
Author: Jofin Joseph
Category: Emotion

<img alt="image alt" src="http://cdn.blog.profoundis.com/people_sentiment.jpeg" width="50%">

*Business is all about People and Sentiments!*

An important part of our information-gathering behavior has always been to find out what other people think. With the growing availability and popularity of opinion-rich resources such as online review sites and personal blogs, new opportunities and challenges arise as people now can, and do, actively use information technologies to seek out and understand the opinions of others. The sudden eruption of activity in the area of opinion mining and sentiment analysis, which deals with the computational treatment of opinion, sentiment, and subjectivity in text, has thus occurred at least in part as a direct response to the surge of interest in new systems that deal directly with opinions as a first-class object.

But is that all? Do you give equal importance to your million dollar customer and your 100 dollar customer? Will the impact be the same when a film star with millions of followers speaks about your brand and when a common man with 100 followers speak about your brand? 

Yes while sentiments ARE important, equally important is the question 'Whose sentiments?' People mapping turns the social figures or people into a web of interconnected people where each person is rated for his influence (Klout is already doing this). The influencer level of the person speaking about your brand directly reflects in the impact of a mention. 

'Emotion' brings to you an 'Emotion Score' of your organization which is not mere number of positive and negative mentions but an index of the impact the mentions are making too. 

How can people be separated when we think of emotions! 
