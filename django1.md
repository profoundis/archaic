Date: 2012-09-23
Title: Django-The Web Framework for perfectionists with deadlines
Tags: Profoundis, Startup Village, Dream, Change, 30 Days to Freedom
Slug: The-Web-Framework-for-perfectionists-with-deadlines
Author: Nibil Ashraf
Category: General

<img alt="image alt" src="http://cdn.blog.profoundis.com/django.png" width="50%">

"The Web Framework for perfectionists with deadlines". This is the slogan for Django, a web framework designed to help you build complex web applications simply and quickly. It’s written in the Python programming language and is its top notch web framework. It follows the model–view–controller architectural pattern. Python community has its own unique character. Python has a culture which finds an ideal balance between fast-moving innovation and diligent caution. It emphasizes readability, minimizes "magic," treats documentation as a first-class concern, and has a tradition of well-tested, backward-compatible releases in both the core language and its ecosystem of libraries. It blends approachability for beginners with maintainability for large projects, which has enabled its presence in fields as diverse as scientific computing, video games, systems automation, and the web.

Django was developed over a two year period by programmers working for an online news operation, the Lawrence Journal-World Online in Lawrence, Kansas. Eventually, the team realized it had a real framework on its hands and released the code to the public under an open-source BSD license in July 2005. The Django framework was named after the famous Belgian guitarist **Django Reinhardt**.

From June 2008, Django is maintained by Django Software Foundation. Once it became a community project, the development took off and Django began to pop up all over the web.  You can obtain the complete list form [Django sites][1] . Notable examples include [Pownce][2], [The Washington Post][3] , [Everyblock][4] , [Mozilla][5] and [Instagram][6].The latest release of django is 1.4.1 and it requires [Python][7] 2.5 and higher.You can get details about Django Installation from [Django Installation Guide][8]. One of the main features of Django is its self explanatory [tutorial][9]. [Django book][10] is also a reliable source for beginners. Programmers who are interested in contributing can download the code by using Git. Go to your terminal and type the command `git clone https://github.com/django/django.git` to get the Django repository. Check out and have fun with [Django][11].


  [1]: http://www.djangosites.org/
  [2]: http://http://pownce.com/
  [3]: http://www.washingtonpost.com/
  [4]: http://www.everyblock.net/
  [5]: http://www.mozilla.org/en-US/
  [6]: http://instagram.com/
  [7]: http://www.djangobook.com/en/2.0/index.html
  [8]: https://docs.djangoproject.com/en/1.4/intro/install/
  [9]: https://docs.djangoproject.com/en/1.4/intro/tutorial01/
  [10]: http://www.djangobook.com/en/2.0/index.html
  [11]: https://www.djangoproject.com/
