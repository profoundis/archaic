Date: 2012-07-6
Title:Steps to install and configure OpenERP 6.1 in ubuntu with nginx
Tags: Profoundis,OpenERP,installation,OpenERP customization
Slug: openERP_installation
Author: Nithin Sam Oommen
Category: OpenERP

*As of OpenERP 6.1, the web client is included as part of the standard all-in-one
installation, so no extra step is required to install it. The OpenERP 6.1 web client is automatically available as soon as the OpenERP server is running.It is possible to install OpenERP with  popular linux distributions (Red Hat, CentOS, Debian), but the package is optimized for Ubuntu by default.*


*I am running this installation as root user. But it is better to run it as a normal user  and use the root privileges via sudo command whenever necessary.*

***Step 1# update the server***

    root@LinuxServer:~# apt-get update
    root@LinuxServer:~# apt-get dist-upgrade

***Step 2# Create a user that will own and run the OpenERP application.***

    root@LinuxServer:~# adduser --system --home=/home/openerp --group openerp

***Step 3# Now install and configure Postgrestql server(Database Server)***

    root@LinuxServer:~# apt-get install postgresql
    root@LinuxServer:~# apt-get install pgadmin3

***Reset the password of the postgres user***

*The default superuser, called ‘postgres' and does not have a password by default. To add a new password,*

    root@LinuxServer:~# su postgres -c passwd

*You will be asked for a new password*

***Now change to the postgres user so we have the necessary privileges to configure the database.***

    root@LinuxServer:~# su - postgres

*Create a new database user so that OpenERP has access rights to connect to PostgreSQL and to create and drop databases. Also remember that you are going to need the password you enter here.*

    postgres@LinuxServer:~$ createuser --createdb --username postgres --no-createrole --no-superuser --pwprompt openerp
    Enter password for new role: ********
    Enter it again: ********

*Finally exit from the postgres user account:*

    postgres@LinuxServer:~$ exit

***Step 4# Now install the necessary Python libraries for the server***

    root@LinuxServer:~# apt-get install python-dateutil python-feedparser python-gdata python-ldap python-libxslt1 python-lxml python-mako python-openid python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-vatnumber python-vobject python-webdav python-xlwt python-yaml python-zsi
    root@LinuxServer:~# apt-get install python-pip
    root@LinuxServer:~# pip install werkzeug
    root@LinuxServer:~# cd /usr/local/src/
    root@LinuxServer:/usr/local/src# wget http://nightly.openerp.com/6.1/releases/openerp-6.1-latest.tar.gz
    root@LinuxServer:/usr/local/src# tar -xvf openerp-6.1-latest.tar.gz
    root@LinuxServer:/usr/local/src# chown -R openerp: *
    root@LinuxServer:/usr/local/src# cp -a openerp-6.1-XXXXXXXX-XXXXXX /home/openerp/server

***Step 5# Configuring the OpenERP application***

    root@LinuxServer:/usr/local/src# cd /home/openerp/
    root@LinuxServer:/home/openerp# cp server/install/openerp-server.conf /etc/
    root@LinuxServer:/home/openerp# chown openerp: /etc/openerp-server.conf
    root@LinuxServer:/home/openerp# chmod 640 /etc/openerp-server.conf

*Open the /etc/openerp-server.conf file and
change the 'false' in the line 'db_password = False' to the same password you used back in the line "Enter password for new role: ######"
And include the following line to specify the log file.
logfile = /var/log/openerp/openerp-server.log*

*Now create the log directory*

    root@LinuxServer:/home/openerp# mkdir /var/log/openerp
    root@LinuxServer:/home/openerp# chown openerp:root /var/log/openerp

*Now, Let's start the openERP server for testing.*

    root@LinuxServer:/home/openerp# su - openerp -s /bin/bash
    openerp@LinuxServer:~$ /home/openerp/server/openerp-server

*If you see few lines saying OpenERP is running and waiting for connections then everything is fine. Just type CTL+C to stop the server and exit the openerp user’s shell.*

*If there are errors, you’ll need to go back and check where the problem is.*

***Step 6# Installing the boot script***

    root@LinuxServer:/home/openerp# wget http://cdn.blog.profoundis.com/openerp-server
    root@LinuxServer:/home/openerp# mv openerp-server /etc/init.d/openerp-server
    root@LinuxServer:/home/openerp# chmod 755 /etc/init.d/openerp-server
    root@LinuxServer:/home/openerp# chown root: /etc/init.d/openerp-server


***Step 7# Testing if the init script is working.***

*To start the OpenERP server type:*

    root@LinuxServer:/home/openerp# /etc/init.d/openerp-server start
*You should now be able to view the logfile and see that the server has started.*

    root@LinuxServer:/home/openerp# tail /var/log/openerp/openerp-server.log

***If the log file looks OK, we can automate the script and then install our nginx webserver.***
    
    root@LinuxServer:/home/openerp# update-rc.d openerp-server defaults
    root@LinuxServer:/home/openerp# vi /etc/apt/sources.list
*Add the following lines*

    deb http://nginx.org/packages/ubuntu/ lucid nginx
    deb-src http://nginx.org/packages/ubuntu/ lucid nginx
*Now install nginx*

    root@LinuxServer:/home/openerp# apt-get update 
    root@LinuxServer:/home/openerp# apt-get install nginx
    

*Open the nginx configuration file* 

    root@LinuxServer:/home/openerp# vim /etc/nginx/nginx.conf
    
*Add the following lines. Do not forget to replace "erp.yourdomain.com" with the specific domain through which you wish to access the OpenERP.*


    server {
        listen 80;
        server_name erp.yourdomain.com;
        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;
        location / {
            proxy_pass http://127.0.0.1:8069;
            }
    }

 *Save the file and Restart the nginx server.*

    /etc/init.d/nginx restart
*Open the url
**http://erp.yourdomain.com** in a browser and you will see a login page like this.*

<img alt="image alt" src="http://cdn.blog.profoundis.com/openerp.png" width="80%">

We at **[profoundis][1]** delivers OpenERP implementations and customizations. 
<a href="mailto:contact@profoundis.com">Contact Us</a>

  [1]: http://www.profoundis.com

