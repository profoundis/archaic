Date: 2012-07-20
Title: Analytics is NOT Formatted Reporting
Tags: Analytics, Business analytics, Reporting
Slug: Analytics is NOT Formatted Reporting
Author: Arjun R Pillai
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/analytics1.png" width="80%">


Is your Business Analytics or Business Intelligence providing you nothing, but formatted reporting, then this is a reminder to you. Instead of just measuring business results, after they’ve been achieved, which is the function that current ‘Reporting Business Analytics’ does, the ‘Actual Business analytics’ will advise and drive the business forward with an arsenal of analyses and tools for realtime decision making. These will be delivered with a view to improving earning power and efficiency, and they will be delivered to people in all corners of the organization and even outside it. There are N distinugishing factors, however we will speak about the most relevant few. 


**The ordinary reports misses INSIGHTs**

Analytics, as the term rightly suggest, is expected to provide the ability to thoroughly analyze the data and provide understanding about operational performance of the enterpirse. This typically requires more than mere tables and bar charts showing monthly averages. The Standard reports do not provide customized information relevant to the individual users. When coupled with quality improvement frameworks, Anaytics can provide powerful visualizations to determine if performance and/or quality is stable, improving, or getting worse. Machine learning algorithms can scan large volumes of data to identify patterns that may not be detected otherwise. Analytics can provide more insight into data than is possible with standard reporting.

**Analytics facilitates Solid decision support**

Not only can analytics be useful for analyzing previous and/or current performance, the branch of forecast/predictive analytics will help the enterprises to glance into the future. Executives can use predictive Analytics to better gauge future demand for products/services, which results in more efficient and effective resource planning, inventory planning, utlization and financial forecasting. 

**Analytics prompts action** 

The Standard reporting takes time to be understood and the quick action may get delayed. The dashboards may look like colourful reports, but well-designed dashboards help to draw immediate attention when processes or performance fall outside of acceptable limits. What might get missed in standard reports becomes obvious when conditionally highlighted in a split colour on a dashboard, or when an email alert is sent out by an automated monitoring agent. Analytics catalyses speedy actions as soon as they are necessary, not when somebody finds a needle in the haystack combing through a report.

These are very high-level of some of how analytics is more than “just” reporting. Analytics are, in fact, a key component in providing vital, up-to-date information in a variety of formats, spanning from past to future performance, to leaders and other stakeholders concerned with making decisions in the operation (and optimization). 

