Date: 2012-07-05
Title: Business Analytics - Growth Driver for the Future
Tags: Business Intelligence, Business Analytics, SME, SMB, Small and Medium Industries, Intelligence, Analytics, Business
Slug: BI-Growth-Driver-For-Future
Author: Jofin Joseph
Category: Analytics

Business analytics is changing in scale and complexity. The term business intelligence (BI) was coined back in 1958, when IBM researcher Hans Peter Luhn used it in an IBM Journal article. Since then, business intelligence has developed as a concept and towards a full fledged field of research. 

In the initial years, business intelligence was about understandable presentation of data. Later on, business intelligence grew to be much more than the presentation of data. Combination of data analysis and statistics initiated predictive analysis, the forecasting and future prediction. 


Analytics is moving out of the IT function and into business marketing,  research and development. Present day analytics systems serve multiple purposes. 

 - Reduction in operating expenses
 - Increased profitability Improve
 - growth, competitiveness and market
 - position Customer acquisition,
 - loyalty and retention Product
 - development and differentiation

In most IT organizations it takes several months to build and deploy a high end solution. This is of course not the desirable time to live  for a business user who needs to analyse data and take smart decisions. Hence cloud-as-a-analytics alternative is gaining traction with business users. Go big, deep and cheap has become the focus of the analytic tools. 

As analytics becomes cheaper and smarter, the future belongs to the companies who harness the pace of the development by using smarter analytics solutions
