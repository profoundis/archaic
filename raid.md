Date: 2012-06-5
Title:Know RAID
Tags: Profoundis,Raid
Slug: knowraid
Author: Nithin Sam Oommen
Category: Linux Administration 

RAID
----
(Redundant Array Of Independent Disks) is a storage technology that combines multiple disk drive components into a logical unit. Data is distributed across the drives in one of several ways called "RAID levels", depending on what level of redundancy and performance (via parallel communication) is required.

Levels of RAID
--------------
***RAID 0***
![image alt][1]

 - Minimum of 2 disks.
 -  Excellent 
   performance( as blocks are striped 
   ).
 - No redundancy ( no mirror, no parity ).
 -  Don’t use this for any  critical system.

***RAID 1***
![image alt][2]
 - Minimum 2 disks.

 - Good performance ( no striping. no parity ).

 - Excellent redundancy ( as blocks are mirrored ).

 

***RAID 2***

 - This uses bit level striping. i.e Instead of striping the blocks across the disks, it stripes the bits across the disks.
 - **This is not used anymore.** This is expensive and implementing it in a RAID controller is complex, and ECC is redundant now-a-days, as the hard disk themselves can do this.
 

***RAID 3***
![image alt][3]
 - This uses byte level striping. i.e Instead of striping the blocks across the disks, it stripes the bits across the disks.

 - Uses multiple data disks, and a dedicated disk to store parity.

 - The disks have to spin in sync to get to the data.
Sequential read and write will have good performance.
Random read and write will have worst performance.
 - **This is not commonly used**.

 ***RAID 4***
![image alt][4]
 - This uses block level striping.
Uses multiple data disks, and a dedicated disk to store parity.
 - Minimum of 3 disks (2 disks for data and 1 for parity)
 - Good random reads, as the data blocks are striped.
Bad random writes, as for every write, it has to write to the single parity disk.
 - 
It is somewhat similar to RAID 3 and 5, but little different.
This is just like RAID 3 in having the dedicated parity disk, but this stripes blocks.
This is just like RAID 5 in striping the blocks across the data disks, but this has only one parity disk.

 - **This is not commonly used**.

 
***RAID 5***
![image alt][5]

 - Minimum 3 disks.
 - Good performance ( as blocks are striped ).
 - Good redundancy ( distributed parity ).
 - Best cost effective option providing both performance and redundancy. 
 - This is good for DB that is heavily read oriented. Write operations will be slow.
***RAID 6***
![image alt][6]
 - Similar to RAID 5, RAID 6 does block level striping. However, it uses dual parity.
 - 
This creates two parity blocks for each data block.
 - Can handle two disk failure.
 - This RAID configuration is complex to implement in a RAID controller, as it has to calculate two parity data for each data block.


***RAID 10***
![image alt][7]

 - RAID 10 is also called as RAID 1+0
 - Minimum 4 disks.
 - This is also called as “stripe of mirrors”
 - Excellent redundancy ( as blocks are mirrored )
 - Excellent performance ( as blocks are striped )


**For critical production servers, RAID 5 or RAID 10 is better.**


A comparison between Software RAID(OS level) and Hardware RAID
--------------------------------------------------------------
<table border='1' style="border-collapse:collapse;">
<tr>
<b><td><b>Features</b></td>
<td><b>Software RAID</b></td>
<td><b>Hardware RAID</b></td></tr>
<tr>
<td>Cost</td>
<td><center>Low</center></td>
<td><center>High</center></td>
</tr>
<tr>
<td>
<p><b>Write back cache(BBU)</b></p>
There is no way to add a battery for software RAID.
Hardware RAID can run in write-back mode if it has a Battery Back Up installed so that pending writes are not lost on a power failure.</td>
<td>No</td>
<td><center>Yes</center></td>
</tr>
<tr>
<td>Complexity</td>
<td><center>Medium/High</center></td>
<td><center>No</center></td>
</tr>
<tr>
<td>Performance</td>
<td>Depends on the server CPU performance and current load.</td>
<td><center>No</center></td>
</tr>
<tr>
<td>Resources (CPU, RAM etc..)</td>
<td>The more hard drives means more CPU cycle will go to software RAID instead of your Web Server, Mails Servers.</td>
<td><center>Not used</center></td>
</tr>
<tr>
<td>
<p><b>Disk hot swapping:</b></p>
Replacing hard disk without shutting down the server.</td>
<td><center>No</center></td>
<td>Yes,
Many RAID controller supports disk hot swapping.</td>
</tr>
<td>
<p><b>Can act as a backup solution?:</b></p>
Both software and hardware RAID cannot protect you against human errors or viruses.But it can protect your data against disk failures depending on the level of raid used. Daily scheduled and off site backups of your system are highly recommended.</td>
<td><center>No</center></td>
<td><center>No</center</td></table>

[1]: http://cdn.blog.profoundis.com/raid0.png
[2]: http://cdn.blog.profoundis.com/raid1.png
[3]: http://cdn.blog.profoundis.com/raid3.png
[4]: http://cdn.blog.profoundis.com/raid4.png
[5]: http://cdn.blog.profoundis.com/raid5.png
[6]: http://cdn.blog.profoundis.com/raid6.png
[7]: http://cdn.blog.profoundis.com/raid10.png 
