Date: 2012-09-13
Title: Business optimization for a Medium scale manufacturing organization
Tags: Profoundis, Analytics, Business Optimization, Social Behaviour, Business Transactions, Reports
Slug: Business-optimization-for-a-Medium-scale-manufacturing-organization
Author: Arjun R Pillai
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/speedometer.jpg" width="50%">

**Abstract**

A medium scale manufacturing company was looking to optimize the business processes

**Challenge**

The company had in place an accounting software package. But there was no integration between the sales or 
purchase data and the accounting details. The sales and purchase data were kept largely in spreadsheets and fed to 
the accounting software at a later stage. This was posing data integrity constraints as well as low productivity. The 
company wanted to analyze the performance of the company. The following questions were raised:

**Time**

 - Comparison study between this month and last month
 - Comparison study between this year and last year
 - Comparison study between this quarter and last quarter
 - Best-selling festive seasons

**Products**

 -  Best-selling product
 - Maximum profitable product
 - Percentage of revenue from each category of products

**Customers**

 - Category and type of customers
 -  Repeated customers (online and offline)
 - Combination products purchased by customers
 - Order delivery for the customers

**Location**

 - Bestselling location
 - Location based trend analysis of sales

**Profoundis solution:**

Profoundis deployed the comprehensive ‘Momentous’ Business Suite 
after specific customization for the customer. The suite included the following modules:

 - Sales
 - Purchase
 - Manufacturing
 - Accounting
 - Human Resources

**Result**

Processes in place

 - Reduced inventory, maximum utilization of resources
 - Reduction in lost sales due to effective delivery on demand (No shortage)
Customer satisfaction
 - Improved on-time delivery
 - Efficient tracking of shipping
The business optimization of the organization was efficiently completed. The production process was optimized to 
avoid piling up of stock and unavailability of stock.

<img alt="image alt" src="http://cdn.blog.profoundis.com/arrow.jpg" width="75%">
