Date: 2012-11-5
Title: The Power of Visualization
Tags: Profoundis, Analytics, Visualisation, Business Analytics, Intelligence, Reports, Statistics
Slug: the_power_of_visualization 
Author: Arjun R Pillai
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/image1.jpeg" width="50%">

Charles Minard, French civil engineer, illustrated using visualization the disastrous result of Napoleon's failed Russian campaign of 1812 on Moscow. As you can see in the picture below, Minard’s graphic uses a lightly shaded bar to illustrate the size of the advancing army. The bar’s thickness progressively declines as the army makes its way toward Moscow. Below, a black bar shows the retreat from Moscow and subsequent army’s decline in strength. 

The above graph is plotted aligned with temperature variation at the bottom of the graphic as a line, which tracks the outside temperature, which was terribly damaging to the size of the troop. The middle of the graph, the black bar grows briefly larger showing the rejoining of a flanking group that had earlier broken off from the main group. The bar grows smaller when the group crosses a river drawn - a testimony to the effect of the icy waters. The shaded and black bars finally meet back at the beginning of the march and the viewer can clearly see how a once large force was reduced to a handful of troops. The simple chart explains the whole story in less than a minute. 

This example is about the power of Visualization – whether used in a right or wrong way. American writer Darrell Huff published ‘How to Lie with Statistics’ in 1955. Below is an example depicting the concept Huff projected. The chart on the left represents a truthful visualization of data, while the chart on the top right has removed the zero, thus showing a more dramatic climb in spending. On the bottom right, the chart is modulated again to convey some alarming figures. Huff explained the power of Visualization – and how effectively the Statistical presentation should be chosen.

<img alt="image alt" src="http://cdn.blog.profoundis.com/image2.jpeg" width="50%">

