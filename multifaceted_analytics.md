Date: 2012-11-15
Title: The Multifaceted Analytics
Tags: Analytics, Multifaceted Analytics, data mining, Real-time analytics, Reporting, Advanced analytics, Customers, Churn, Action, Insight, Prediction, Forecast, Statistics
Slug: the_multifaceted_analytics
Author: Arjun R Pillai
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/Multifaceted_by_ART_BY_DOC.jpg" width="25%">

The Multifaceted Analytics

 - **Reporting**

     • Evaluate what happened after the fact, and to thereby trigger
corrective actions

 - **Ad hoc queries**, 

    • User-selected queries, and OLAP is used for such activities as
deeper analysis into why something happened.

 - **Advanced analytics**

   • Advanced statistical & data mining techniques used to
discover why something happened.

 - **Predictive analytics**

 • Forecast what is likely to happen and the likely economic results;

 - **Real-time analytics**

 • Rein on minute-by-minute performance of some key business
process.

