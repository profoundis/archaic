Date: 2012-09-15
Title: Analytics can make a change
Tags: Analytics, Government Projects, scope of business analytics 
Slug: analytics-can-make-a-change
Author: Nibil Ashraf
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/vision_trithala.jpg" width="40%">
Analytics can make great differences in every field. We have seen its numerous applications and implications in real life around. Here we can see how it changed the face of two constituencies. Susthira Puthukkad and Vision Trithala are two projects designed and launched for Puthukkad and Trithala constituencies located in Thrissur and Palakkad districts of Kerala, India respectively.

Susthira Puthukkad is a project which made a complete study of resources and requirements of the Puthukkad Constituency. Through that study, they came to know that what all are the needs of the people and what they can offer. One of the best implementation of this project is the Cultivation of the special kind of Banana - 'Kadalippazham' needed for Guruvayoor Sri Krishna Temple. Earlier the whole fruit needed were coming from Tamil Nadu and other states. Now Puthukkad is able to offer the whole Kadalippazham required by the temple This is an excellent example for the application of Analytics. The analysis weighed the current stats and the capabilities. The focused actions helped the constituency to achieve the full potential.

Vision Trithala is a recent 10 year project which aims at the development of the constituency. It master plans the development in the fields of Education, Conservation of Nature, Small Scale Industries, Fundamental development and Healthcare after a thorough analysis of the strengths and weakness in each sectors. Business Analytics is the back-bone that answers all the questions. This analysis has revealed the unexplored sectors and has changed the pace of development.

