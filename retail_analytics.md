Date: 2012-09-29
Title: Need For Business Intelligence In Retail
Tags: Profoundis, Analytics, Retail Analytics
Slug: need-for-business-intelligence-in-retail
Author: Nithin Sam Oommen
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/future-of-retail.jpg" width="50%">

Retail industry is driven by a lot of questions.

**Consumers ask them:**
 
 - Which is the store that I should shop from?  
 - Do I really need that item? 
 - Is it affordable for me?

**Retailers in turn ask lots and lots of questions:**

 -  What’s selling these days, and what will be selling items next month? what about Next year?
 -  Who are my most valuable and least valuable type and category of customers? 
 - How do we increase margins at a product-level? What is the best way to target individual customers?
 - What marketing strategies should we employ? 
 - What should be the best price for a product through its lifecycle? 
 - What promotions and offers do we employ in each store? 
 - How do we decide on the product-mix for each store? 
 - How do we ensure effective stock deliveries during busy trading seasons? 
 - Do I have the right number of employees at  my store?


Retailers with the right answers to  hundreds of questions like these – or at least those with 
a high “batting average” – will be successful.The business intelligence (BI), based on a rapidly expanding pool of data sources which can include social 
networks and location-based information from consumer mobile devices, sensor-equipped 
digital signage and likewise can 
provide answers to these questions


With a lot of issues to tackle, retailers need to relook at the way business is typically run to ensure survival. This not only involves re-examining interactions with customers but also focus equally on back- end operations such as store planning, merchandizing, supply-chain infrastructure, and logistics to guarantee success.
 
Business analytics can help retailers break-down and assimilate information collected and provide better insights into customer tastes and preferences as well as back-end processes.

