Date: 2012-04-12
Title: First Post on Profoundis
Tags: Profoundis, awesome
Slug: first-blog-post
Author: Anoop Thomas Mathew
Category: Technology

This is the first blog post ever on the Profoundis blog. Hoping the content would be useful for the rest of the world. Will be adding contents related to work, life, open-source and philosophy. We are a group of techno-management enthusiasts, wanting to make the world a better place.

Welcome to Profoundis!!!
