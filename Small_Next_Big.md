Date: 2012-06-22
Title: Small Is the Next Big
Tags: Business, SME, Business
Slug: Small is the next Big
Author: Jofin Joseph
Category: Business


**Small Is The New Big**
------------------------

When the world turns its focus to the developing economies, the trend of business is changing fast. The growth story of large economies like China, India and Brazil - countries with large population and geographies have been powered by the small scale business sectors. 

India is the classical example of this where 90% of the industrial units are in the small and medium scale sector and 40% of output of the country is from the SME sector. But how promising and futuristic are the SME sector of these countries? How far can these companies make it to *make it large*? 

SMEs not only is a financial entity in the large developing economies, but is also an entity of social balance. The growth of each SME brings in balance in the societies and help attain a '*socialism in a practical way*' It is not huge companies that these economies need, but it is small industries which ensure the even distribution of wealth that is needed. 

**The Opportunity**

 - The funding required to start and
   bootstrap the SMEs are minimal which
   makes them attractive.
 - Ample support from the government and
   SME friendly policies
 - Wide availability of manual skill
   attributed to the large popuation
 - Low risks involved in setting up
   industrial units
 - Ever increasing customer base and
   market

**The Challenge**

 - Quality of products and services
 - Failure of tapping the opportunities overseas
 - Failing in taking the business to the
   'next level'
 - Lack of organised planning and
   process and skilled management
 - Failure to adopt a unified process.

**The Future**

If there is a most effective solution to the challenges faced by small and medium scale businesses, it is TECHNOLOGY. Use of computers and technology was the big wave in the business area which helped businesses optimize, enhance and power up the processes and growth engines of the industries across the world. The advanced tools and technologies however stayed largely out of the reach of small businesses due to huge licences and cost. With the advent of open source technologies and SAAS, now high performance software are within the reach of SMEs as well. 

Powered by technology, fueled by the skill, the SME sector is all set to make it large. Lets sing the saga of *'practical socialism'*