Date: 2012-11-30
Title: Analytics can be Descriptive, Predictive or Prescriptive
Tags: Business Analytics, KPIs, Dashboards, charts,Predictive Analytics,Descriptive Analytics,
Slug: analytics_can_be_descriptive_predictive_ or_prescriptive
Author: Arjun R Pillai
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/prescriptive_analytics.jpg" width="50%">

Businesses are recognizing the role of Business Analytics in getting them an edge
over their competitors. As we talk - Data is exploding – the spread of analytics is
increasing. New Meanings are being defined – New terms are being coined.

The way of usage of Analytics depends on the data maturity of the organizations
employing it. There are:

 **Descriptive**: The Traditional BI which analyses the historical data and
tell you “What happened?”, “How many customers we lost?”, “Why is it
so?”. The reactive way of analytics uses KPIs, Dashboards, charts and
Scorecards to tell you the story.

 **Predictive**: It goes a step after Descriptive Analytics. After analysis the
historical data, we build Statistical Models to predict the future with an
agreeable fuzziness – We predict the future from the past. It answers
questions like “What will happen next month?”, “how many customers
will churn?”, “What if we change pricing?” etc. Besides Statistical
modeling, we use Data mining, forecasting and predictive modeling here.

 **Prescriptive**: Analytics makes sense only when it is combined with
Business Strategies and goals and it helps enterprises to achieve them.

Prescriptive Analytics uses advanced statistical optimization & simulation
techniques with inputs & constraints to recommend what actions to
be taken. This answers questions like “What is the best course of action
to avoid the situation?”, “What action can we take to prevent customers
from churning out?”

There are enterprises that employ these 3 different levels of analytics. The
sooner the enterprises climb the level to prescriptive, the better. At Profoundis
Labs, Our Analysis and R & D wing work continuously to deliver Analytics
solutions at the 3 levels to our Clients.

