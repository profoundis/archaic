Date: 2012-09-10
Title: Financial Analytics - See stories in your numbers
Tags: Analytics, Financial Analytics, Profoundis, Tips, Business Analytics
Slug: financial-analytics-see stories-in-your-numbers
Author: Jofin Joseph
Category: Analytics

Financial analytics is increasingly finding popularity among companies that are looking to better their financial value proposition and optimise business performance. What is Financial analytics? 

Financial analytics is a set of tools or a system that can be used to increase a company's financial productivity, specifically its profitability. Some particular aspects that financial analytics may take into account include which customers provide the company with the most profit, how the company's customer-base spread out geographically, and which product brings in the most profit. 

Leading financial services firms have been early users of financial analytics and have made investments in this area to address their business imperatives. These could be around building a credit line strategy for a consumer banking scenario; developing a robust equity research model by using various fundamental analyses and cash flow analyses; doing claims' severity and frequency analysis in the insurance sector; or building fraud detection models.

Financial executives must now think beyond the traditional financial information contained in general ledger systems and consider how best to provide for the comprehensive measures and analytical methods needed to drive decisions throughout complex, dynamic companies. 

***Tips* Financial Analytics**
<img alt="image alt" src="http://cdn.blog.profoundis.com/financial_analytics.jpg" width="50%">

The *Tips* financial analytics service from Profoundis offers an integrated approach to financial analytics where the reports provide a hawk eye view of the finances and how it is affected by the other functions in the company. 
