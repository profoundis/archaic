Date: 2012-09-14
Title: A Beauty parlor chain identifies optimum pricing and customer relation management
Tags: Profoundis, Analytics, Business Optimization, Social Behaviour, Business Transactions, Reports
Slug: Salon_Chain_Analytics
Author: Arjun R Pillai
Category: Analytics

**Abstract**

The chain was experiencing a reduced level of revenue which 
majorly attributed because of the change in the customerneed pattern and increase competition.

**Challenge**

 - The challenge was to identify the customer pattern and thereby bridge the need-service gap. 
 

 - An edge for the organization to overcome the competition


**Profoundis solution.**

 <img alt="image alt" src="http://cdn.blog.profoundis.com/growth.png" width="40%">

The organization believed that segmentation of customers, targeted pricing may improve the revenue. But 
the evidence to back the thought was missing.

Profoundis analyzed the situation from the existing data and correlated the different service lines  and 
figured the trends. The pricing changes were applied on 4 out of the 13 services and in 5 outlets. The 
piloting has been carefully monitored for a period of time. The growth is revenue proved the model to be a 
success and the same was extrapolated across all the retail outlets.
 
The CRM module from Profoundis helped the company to establish a personal care relation with the 
customers and offer personalized discounts and services based on their history of the customer. 

**Result**

A growth of 2.3% was achieved in just over a year which is expected to 
further improve.

