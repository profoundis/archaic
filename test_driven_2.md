Date: 2012-05-10
Title: Test Driven Development in Python: Part II
Tags: Development Methodology, Testing, Python, TDD
Slug: test-driven-development-ii
Author: Anoop Thomas Mathew
Category: Development

This is the continuation of Test Driven Development in Python: Part I. If you are new to Test Driven development, please read Part I, to get a clear idea about the topic. We'll discuss another example in this post. Doing test driven development for implementation of greater than function is given below.

The requirement is crazy, to compare the *ascii sum of the text string* if argument is a string.

**The Test**

    import unittest
    from demo import Greater
    class DemoTest(unittest.TestCase):
        def test_number(self):
            comparator = Greater()
            result = comparator.greater(10,5)
            self.assertTrue(result)
        def test_char(self):
            comparator = Greater()
            result = comparator.greater('ABCabcxyz', 'ABa')
            self.assertTrue(result)
        def test_char_equal(self):
            comparator = Greater()
            result = comparator.greater('4', 3)
            self.assertTrue(result)
    if __name__ == '__main__':
        unittest.main()


**Now the Code**


    class Greater(object):
        def greater(self, val1, val2):
            if type(val1) ==str or type(val2) == str:
               val1 = str(val1)
               val2 = str(val2)
               sum1 = sum([ord(i) for i in val1])
               sum2 = sum([ord(i) for i in val2])
               if sum1 > sum2:
                   return True
               else:
                   return False
            if val1>val2:
                return True
            else:
                return False

The function returns True or False, based on the ascii values if any of the argument is  string, else give the greater.



