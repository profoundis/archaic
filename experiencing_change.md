Date: 2012-09-27
Title: 3 months of Experiencing CHANGE
Tags: Profoundis, Startup Village, Dream, Change, 30 Days to Freedom
Slug: experiencing-change
Author: Jofin Joseph
Category: General



They say Entrepreneurship is a journey. For us, we are travellers who have just set out for that long journey. When we look around us, we see lots of like minds, some cheering us, some gearing up for the journey and some already speeding up. 

For Profoundis, the journey of entrepreneurship had been one of CHANGE and LEARNING.  

**Our Philosophy**
<img alt="image alt" src="http://cdn.blog.profoundis.com/Profoundis.jpg" width="50%">

At Profoundis, our aim is to build revolutionary products that will have long lasting impact. We want to grow by helping others grow. As a team having worked with major MNCs, the market we identified to start with was not the fortune 100s but it was the small and medium scale industries - A market wich is HUGE in potential, but of course difficult to hit. Profoundis wanted to make an impact on the way businesses work. The first child of the thought process would be ***"Tips"***, the most advanced and SME friendly business analytics suite till date. 

**The Startup Village Ecosystem**

<img alt="image alt" src="http://cdn.blog.profoundis.com/startupvillage.jpg" width="50%">

Lucky enough to be part of the dynamic startup eco-system, Startup Village (startupvillage.in), has given us the initial momentum. Kerala is going through a change. A change in mindset and a change in entrepreneurial spirit and being part of the change excites us. (https://www.facebook.com/30daystofreedom). 

**The way forward**

We are growing and more excitingly we are watching a 100 others like us grow with us. With our solutions we want to watch millions grow and cherish. That is the way forward for Profoundis!!
