Date: 2013-01-01
Title: Sentiment Analytics – a brief FAQ
Tags: Business Intelligence, Business Analytics, Sentiment analytics, what, how
Slug: Sentiments_FAQ
Author: Arjun R Pillai
Category: Emotion

<img alt="image alt" src="http://cdn.blog.profoundis.com/PastedGraphic-1.jpg" width="50%">

Q: What is a Sentiment?

A: A Sentiment is a thought, view, or attitude, especially one based mainly on emotion instead of reason


Q: What is Sentiment Analytics?

A: It's software for automatically extracting opinions, emotions and sentiments in text.
It allows us to track attitudes and feelings on the web. People write blog posts, comments, reviews and tweets about all sorts of different topics. 
We can track products, brands and people for example and determine whether they are viewed positively or negatively on the web. 

<img alt="image alt" src="http://cdn.blog.profoundis.com/PastedGraphic-2.jpg" width="50%">

Q: Why would somebody want to do this? 

A: It allows business to track:
        - Flame detection (bad rants)
        - New product perception
        - Brand perception
        - Reputation management
        
It allows individuals to get:
    - An opinion on something (reviews) on a global scale
    
Q: How do you do this?

A: Several of the Computings Merge. Predominantly:

*Natural language processing*

It deals with the actual text element. It transforms it into a format that the machine can use.

*Artificial intelligence*

It uses the information given by the NLP and uses a lot of maths to determine whether something is negative or positive: it is used for clustering.

Q: Does web really contain sentiment-related information? Where? 

A: Yes. The Web and its contents are created by Humans and Humans always put in a Sentiment perspective. The Web is really a Sentient Space. 

•	Blogs

•	User comments

•	Review websites

•	Community websites

•	Social Medias
