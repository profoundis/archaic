from fabric.api import cd, run
def deploy():
    code_dir = '/webserver/nginx/blog/archaic'
    with cd(code_dir):
        run("git pull origin master")
    with cd('..'):
        run("/webserver/nginx/blog/blog_env/bin/pelican . -s /webserver/nginx/blog/settings.py -o /webserver/nginx/blog/output/")
        
