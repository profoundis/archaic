Date: 2012-09-30
Title: Predictive Analytics – Next Ripple of Analytics
Tags: Profoundis, Analytics, Predictive Analytics, Social Behaviour, Business Transactions, Reports
Slug: predictive-analytics-next-ripple-of-analytics
Author: Arjun R Pillai
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/predictive-analytics.png" width="75%">

The Volume of Data coupled with the Variety drives the need of increased Velocity of Decision Making. The bes way to catch up with the speed is to  get ahead of the curve – Predict and Fix!

**Challenge of the Day**

- Business Leaders Frequently Make Critical Decisions without Information they need.
- Don’t have the access to the entire organization information.

- Hours and hours spent by workers each week searching and finding relevant information

**Imagine if:**

 - Predict and treat infection in premature newborns 24 hours earlier? – A Physician

 - Adjust credit lines as transactions are occurring to account for risk fluctuations? – A Loan Officer

 - Determine who is most likely to buy a product if given a discounted sale? – A Retailer

 - Predict the customer social behaviour and prevent? churn – Call Center Rep


Predictive analytics can be leveraged universally for every transaction without an analytical expert. Predictive Analytics is a Breakthrough technology that enables Proactive decision making, driving competitive edge. It analyzes patterns found in Historical and present transaction data, surveys, feedbacks etc to predict potential future outcomes. 

