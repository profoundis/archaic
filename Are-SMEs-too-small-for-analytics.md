Date: 2012-07-28
Title: Are SMEs too small for Business Analytics
Tags: Business Intelligence, Business Analytics, SME, Small and Medium Industries, Analytics, Business
Slug: Are-SMEs-Too-Small-For-Analytics
Author: Jofin Joseph
Category: Analytics

![image alt][1]

The wave of analytics in business is fast approaching the small and medium scale sector. Due to various reasons, the SME market is still not completly open to the adoption of advanced predictive analytics methodologies to improve their business. 

The reasons for the inihibition are quite obvious.
 

 - Lack of funds to go for advanced
   analytic tools
 - Lack of manpower to invest in usage
   of analytics tools
 - Unavailability of data in the desired
   format

Major players in the analytics industry had been concentrating on the large sized industries for a long time. As a result of which , the scale of most of the common analytics products available in the market are either too large or unaffordable for SMEs. The situation has since changed due to the advent of analytics products focusing the SME market. The advent of software delivered as SaaS further decreases the cost of adoption of analytics products for SMEs. 

Modern analytics tools have come closer to the normal users. CIOs these days can use the analysis tools to generate reports and insights without technical hurdles and difficulties. Analytics further becomes easier to use with the advent of web based SaaS offerings which can be rendered on any platform including mobile and tablet. This provides the CIOs with a decision support system available at their finger tips. 

The third and most important challenge is the availability of clean data for analysis. Most SMEs keep their data in unstructured formats which make them difficult to be used efficiently. Modern ERP systems solve this issue to a great extent. ERP systems in the SME scale like OpenERP have become a common name in the SME market. Further, the modern analytics tools have the capability of deriving insights from unstructured data as well. 

For SMEs the hurdles are getting cleared out very soon. Analytics will sure begin to play a a vital role in the growth of SMEs in the near future. 

[1]: http://cdn.blog.profoundis.com/Analytics230712.jpg
