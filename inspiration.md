Date: 2013-05-10
Title: We are happy to know that we are inspiring people
Tags: Profoundis,Team Profoundis, Startup village
Slug: We_are_happy_to_know_that_we_are_inspiring_people
Author: Arjun R Pillai
Category: General


Today, we heard from [Anirudh][1], an aspiring Entrepreneur from Hyderabad. He wrote to us telling how Profoundis inspired him to follow Entrepreneurship. Not only that, he has penned down a blog post on his personal blog about how Profoundis inspired him. We are humbled and overwhelmed. One of the key visions of the founding team of Profoundis is to inspire & touch lives of people in a positive way. The very fact that we have inspired an Engineering student is an ultimate satisfaction for us.

An extract from Anirudh’s article: *“Later it started being an unrealistic dream but then there was one thing that attracted me, it was a startup started by a group of people at the startup village in Kochi. It’s none other than ‘PROFOUNDIS.’

When I noticed it for the first time in facebook, it was just like other pages. But later on the updates in the page turned out to be my pass time and they started setting the basement towards my dream.”*

**NB: You can read the blog post [here][2]**

Team Profoundis expresses our gratitude and happiness to Anirudh and wishes all the very best in future as well. 

Team Profoundis!


  [1]: https://www.facebook.com/Anirudh001
  [2]: http://anirudh1712.blogspot.in/2013/05/normal-0-false-false-false-en-in-x-none.html
