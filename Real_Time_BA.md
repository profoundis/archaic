Date: 2012-09-15
Title: Business Analytics - Real Time
Tags: Business Analytics, Real time analytics, BI, BA
Slug: Business-Analytics-Real-Time
Author: Jofin Joseph
Category: Analytics

<img alt="image alt" src="http://cdn.blog.profoundis.com/realtime.jpg" width="50%">

Imagine you are shopping from your usual mega store. At the counter, you swipe your card and along with the bill, comes a free coupon to buy your most favourite choco brownie. The guy behind the scenes is a real time analytics system. As you swipe your card, the analytics system sweeps through your buying history from the shop and comes up with one gift which you might like the most. The system has lots of other parameters to look into before it makes the decision - your age, your spending pattern, marital status and so on. This is Real Time Data Analytics.
 
Most business intelligence (BI) implementations query data that is a few hours or minutes old, representing business processes that have already completed. Even so called "real-time" BI reports are generated using historical data. While this historical view of data has some value, it does not represent the complete picture. Fully-informed business decisions require real-time insight into high-volume streaming data sources, current events, and ongoing business processes.

Real-time BA deals with the real time transactional data of the organization and gives insights in split seconds. The continuous analysis of data and ream time insights empowers the business user with the power to take critical decisions on the go. 

The ***'Tips'*** Analytics solution from Profoundis tries to bring to you the power of real time analysis to improve your business. Get the power of Tips from Profoundis!!
