Date: 2012-06-29
Title: A layman introduction to the Evolution of Software Applications 
Tags: Software, History
Slug: Evolution-of-Software-Aplications
Author: Anoop Thomas Mathew
Category: Computer Philosophy


From the late 40's, computer applications started their journey and is continuing is a wide variety of forms, touching our lives knowingly or unknowingly.

In spite of numerous players in the topic, or countless architecture, industry had to wait till Intel launched 80286, to set the x86 standard, for the proliferation of software applications which can be used from system-to-system, making it an industry standard from consumer applications. In this post, we are trying to give a layman idea about the evolution of computer software, and what we can expect in near future.


**Command Line Programs**

 Its the first generation of applications, and are, as for every other first generation, simple. They are mostly single command at a time, and uses it to fulfill all the application requirements in that particular loop. Mostly, they are keyboard operated, but there are exceptions in the later stage, with mouse capabilities. Being a command line program doesn't mean that they are primitive. Just that, everything started from there.

These programs are distributed as binaries, and also, can be compiled from the source code, specific to the architecture.

**Desktop Applications**

Applications with a graphical interface was to follow. Comes with the pointers and was able to handle multiple programs at the same time, making users more productive. It is a conventional belief that desktop applications are prone to security vulnerabilities and pretty slow.

Inspite of all these issues, desktop applications became very popular towards the end of the century, due to the ease of packaging and distribution. Availability of cross-platform compilers fueled the explosion of desktop based GUI applications, like VLC player, Mozilla FireFox, Blender, LibreOffice to name a few.

**Web Applications**

Any application that caters users needs from a remote server. Web applications boasts on the fact that they are accessible from any location, as long as Internet connectivity is a viable resource. 

This is the most exciting category of all, which provides more with less. Usually uses a Software as a Service model, with a freemium billing model, this is the marriage between productivity and affordability. 

Most popular applications like Facebook, GMail, Twitter, Quora, Pinterest and you name the rest you use over the internet fall into this category.



**Mobile Applications**

As the time passed by, devices became more portable, and the need of mobile applications became pretty obvious. They are designed for less processing power and less power consumption. They started working standalone, and in the recent years, we see a lot of integration with the web. Biggest disadvantage is about the platform specific nature of such applications. Each native application has to be designed ground-up, consuming n times man hours to develop for n platforms.  Android and iOS being the major players in the industry, they natively support java and ObjectiveC as the native language.

**Seamless Applications**

From the advent of this century, there has been attempts to make applications seamless across devices. Mobile Web, trying to make a website look like a native app in a mobile environment was one of such kind of effort. To an extend, if the application is light enough, this attempt has paid off. Another such attempt was from PhoneGap and SenchaTouch, similar but a more comprehensive list of native features like camera and orientation access.

We are witnessing the convergence of technology, user experience and affordability.

We at **[Profoundis labs][1]**, envisions a seamless convergence of the web and mobile world, creating a easy to develop and deploy, single code base, cloud hosted,  using minimal server resources,  web oriented, platform and device independent application class to emerge in the very near future.


   [1]: http://www.profoundis.com		

