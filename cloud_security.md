Date: 2012-06-02
Title: Let’s learn to keep our Information secure in Cloud
Tags: Security, Cloud Data Management, Information Security
Slug: cloud-security
Author: Arjun R Pillai
Category: Information Mangement           

![image alt][1]![image alt][2]

Cloud, the big term, is getting more and more attention both from providers and customers. The recent big news being Google Drive which makes the cloud market even livelier.  As the cloud services are being more and more used by users, the Hacker groups are getting into their act more seriously. The information we store in cloud includes Credit card infos, home addresses, office & personal mail ids and what not. We have been seeing a flurry of attacks over the past couple of years including the once on Facebook, fox.com and Sony’s PS network. Thousands and millions of user accounts got hacked; thousands of users themselves dissolved their account. Well, hackers are the prime culprits, but we always have a major group of culprits – Us – the users. It is not our fault that the site got hacked, but we do have a lot of bad habits when it comes to web security and these places us at risk to attacks.

How often do you use same usernames and passwords? How often do you write down passwords? How often do you share your passwords? How often do you use dictionary and other weak passwords? We do this even with our Online banking accounts. Let’s take some tips to be safe

 **- Choose a tough password if not the toughest**

The password is a lock: The tougher the lock, the tougher to break it. A study after a hacking proved that out of over 30 million examined hacked passwords, thousands of users sustained with the same basic password ‘123456’ followed by ‘12345’ followed by ‘123456789’ followed by ‘Password’ and ‘iloveyou’. Find a safe place to store your passwords if they are difficult to be remembered. It can be a smartphone wallet or online storage wallets (Both are available in plenty).

 **- Don’t re-use or share the password.**

Corporates mandates that the passwords be changed once in 2 or 3 months and the password used over last 6-9 months may not be repeated. Users usually choose 2 passwords and swaps between them. We also use the same password for FB, twitter, Google and bank account. Imagine, cracking into one of them, the hacker will easily get access to all. 

 **- Back up your data**

We all know that we should back up our data. But more often we end up losing our data and blaming ourselves. A power surge, a faulty hard disk, a scratched DVD/CD or any other unexpected failures can result you to beat yourself for losing your most valuable data (documents, photos, videos etc.). 

Cloud storages are coming in different and user friendly packages. DropBox, Google Drive, MS SkyDrive and many more are offering GBs of free storage and cheap space after that. Using these services will not only provide us backup, but also enables us to access our data at any end of the globe. As a good practice, keep your data in one of them, back up in another one. 

**- Be on the guard**

Avoid suspicious and spooky websites if possible. Attachments from unknown user with no mail content are not worth opening. Let’s leave alone the random links seen on FBs and twitters. If you are suspicious, give a Google search about the vendor, website or retailer before committing. Antiviruses and Internet Security can help, but let’s learn to be smart ourselves. 

**- Be aware about the computer and browser you use.**

The computers/browsers can save your login information and can save you session alive. Be cautious when working on a public computer. Make sure you don’t save information or keep login alive. Unless you are using your own PCs, this is the first thing to look out for. 

There are much more things to be said and done. But with these basic stuffs, we can live safe. 

  [1]: http://officeimg.vo.msecnd.net/en-us/images/MH900433145.jpg
  [2]: http://officeimg.vo.msecnd.net/en-us/images/MH900400626.jpg
