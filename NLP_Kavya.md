Date: 2013-01-31
Title: Natural Language Processing - An Outlook 
Tags: nlp,Natural language processing
Slug: natural_language_processing
Author: Kavya
Category: Emotion


<img alt="image alt" src="http://cdn.blog.profoundis.com/natural_language_processing.png" width="25%">

What is 'natural language processing'? For beginners its basically enabling the computers to understand our normal human language, just like two humans can communicate and comprehend each other while they talk, text etc we want computers to be able to communicate and comprehend human language just like another human or even better than a human. Natural language processing (NLP) is a field of computer science that deals with *computer* and *human interaction*. Its main challenge is to enable computers to derive meaning of human language input and its ability to impersonate human communication. The most common example of NLP is the automated assistant we get when call up customer services, iphone voice search etc.


Why do we need NLP? It is estimated that around 85% of the business information exist in the semi structured data( Businesses create a huge amount of valuable information in the form of e-mails, memos, notes from call-centers, news, user groups, chats, reports, web-pages, presentations, image-files, video-files, and marketing material and news). So once this information can be analyzed easily and accurately an informed decision making can be done .But the problem with semi structured data is that it is very informal and searching ability and assessment of such data is very difficult due to absence of standardized terminology and its vast amount, here NLP becomes very useful as they can comprehend human language which is in fact the informal language in which semi structured data exists.

<center><img alt="image alt" src="http://cdn.blog.profoundis.com/mabi87.png" width="75%"></center>

Applications of NLP include automatic translation of texts (Google's translation service), extracting financial information from news reports, automatically summarizing text, creating new texts by combining information from multiple documents, search engines, converting spoken speech into text and automatic subtitling of videos, word sense disambiguation (selecting the most suitable meaning of a word under a given context), converting information from computer database to human readable form

The challenges which NLPs usually face with text are polysemy Eg: the word bat may mean cricket bat or the mammal bat, synonymy, word order etc.
