Date: 2012-05-09
Title: Test Driven Development in Python: Part I
Tags: Development Methodology, Testing, Python, TDD
Slug: test-driven-development-i
Author: Anoop Thomas Mathew
Category: Development


> We can either prevent bad things from
> happening or fix it, once it is
> detected.

It is your choice to select any of these methodology, while developing a software. You can either develop based on a test driven process or the recover from a fiasco with tests.

Test driven development, as the name suggests, is development based on tests. Tests for core features are written prior to the implementation for the expected output, and then necessary modules are written to satisfy the needs defined in the requirements.


----------



**Advantages of Test Driven Development**

 - application is determined by using it
 - written minimal amount of application code
 - total application + tests is probably more smaller
 - simpler, stand-alone, minimal dependencies
 - tends to result in extensible architectures
 - instant feedback
 - future development won't break existing features.
 


----------



**About Test Driven Development**

 - Write tests for the use case Run it
 - (make sure it fails and fails
 - miserably) Write code and implement
 - the required functionality with relevant level of detail 
 - Run the test
 - Write test for addition features 
 - Run all test Watch it succeed. Have a cup of coffee !



**Basic Unittest**


----------



    import unittest
    class MyTest(unittest.TestCase):
        def testMethod(self):
            self.assertEqual(hello(5), 5, "Hello din't return 5.")

    if __name__ == '__main__':
        unittest.main()

To write a basic unittest in python, is pretty straight forward. The code above is self explainatory. It is trying out hello function, and expected output is given as 5. If somehow, hello() din't return 5, then the error message is printed to the console.


Now, let's try writing the function hello to satisfy the test.

    def hello(value):
        return 5

or

    def hello(value):
        return value

**This is a problem!**

The problem is, how to select the best. Solution is, to get the tests more detailed and covering more test cases to satisfy the requiements of the function. As we have more precise requirements in the tests, we can easily rule out either the more complex solutions or the more simpler solutions, and get the perfect balance between development and requirement.


One thing to keep in mind while doing Test Driven Development is, ***Don't Overkill***. Just write tests for the core features expected, and write it expecting all the complete range. For example, if it is a multiplication function, make sure that you need make sure that you handle strings as you need it.

