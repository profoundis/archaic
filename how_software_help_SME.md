Date: 2012-05-06
Title: What can software do for SMEs
Tags: BI, business
Slug: how-software-help-SME
Author: Jofin Joseph
Category: Business


**What can software do for SMEs**

The terms 'automation' and 'computerization' became buzzwords in the business arena decades back. Since then, the use of technology in the business world has been ever increasing and the growth of technology use has in turn resulted in the growth of business. The business establishments that stood distinct in the business realm are the ones which adopted technology in their functioning. 

Technology itself have been in a phase of quick transformation through these years. From the era of punch card inputs and command line outputs, software has grown to advanced algorithms and programs and infrastructures that can handle huge amount of data. However all the advancements that has come in the Business Software has not reached the small and medium scale industries. The scale of the integrated enterprise software built post the 'accounting software boom' were either too large or un affordable for the SME sector. Thus the use of software technology among SMEs is largely limited to accounting and de-centralized solutions for different business functions. These advanced software packages which presented integrated business optimization solutions stood out of the way of SMEs.

Vendors like SAP and Oracle build high quality enterprise software for the business giants, but unfortunately, the SMEs which make up the lions share of business in any economy were largely deprived of the ERP/BPM solutions. The size of the SME sector is difficult to gauge. 

According to the market study by Tally Solutions, India has an estimated 8 million SMEs capable of leveraging IT for their growth. 
New York-based research firm Access Markets International (AMI) Partners estimates the addressable SME market for IT vendors at four million. 

The one major hurdle that SMEs face when it comes to technology adoption would be the high cost of software. This problem is solved to a great extend with the advent of open source business software packages like OpenERP and the Software as a Service (SAAS) software model. Open Source saves SMEs from the huge licencing costs of business software while SAAS model helps reduce the high upfront cost and running cost of software and delivers software as a 'pay as you go' model. 

The next wave of software technology revolution among SMEs are on the way!! [Profoundis][1] is all set for it with the [Momentous Business Suite][2].


  [1]: http://www.profoundis.com
  [2]: http://www.profoundis.com/?link=services&data=mo
