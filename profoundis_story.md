Date: 2013-04-30
Title: The Profoundis Story
Tags: General, Profoundis, Story, Startup Village
Slug: the-profoundis-story
Author: Jofin Joseph
Category: General

<img alt="image alt" src="http://cdn.blog.profoundis.com/four_of_us.png" width="50%">

There are many reasons people choose themselves to be entrepreneurs. For some it could be money. Some need the exposure and fame. For some business might be a family matter. Some do it to keep themselves occupied. For some it might just be the passion – Passion to do awesome stuff, passion to excel, passion to inspire! 
Profoundis is the brainchild of a group of youngsters, moulded by their passion. The passion to be awesome! Arjun R Pillai, Jofin Joseph, Anoop Thomas and Nithin Sam – a closely knitted team with diverse skills as they call themselves, are the four youngsters. 
In our words

**Where it Started** 

We were classmates at College of Engineering Chengannur, one of the premier institutions in Kerala when it comes to entrepreneurship. CEC as we call the college,  gave birth to close to 50 companies, small and big, in 15 years. Right from the initial days at college, we were inspired and guided by the companies like Poornam and many campus start-ups in the college. The environment at the college was activity driven rather than academic driven. The activities at college brought us close and formed the team. We worked together at the college on various events and programmes from arts and sports days to international conferences. 

Arjun was an organizer and manager and made his mark with his volunteering activities with IEEE. He chaired the IEEE section at college and learned from eminent mentors from IEEE.  Arjun having run the show at the IEEE region 10 student congress at Singapore when he was in 3rd year of engineering, makes his profile matchless. Jofin proved his capabilities in taking responsibilities in management and technical activities together. He was instrumental in getting all events in the college go digital with online management systems for all activities at college. He was a person who would code and build software, arrange judges and speakers for events and even carry water cans to the venues. Anoop Thomas Mathew (known ATM) was the best technology guy the college had ever seen. A coder, animator and designer, he took initiative to bring out a digital magazine while in the first semester. Nithin Sam (fondly called Mamboo) was a master networker and the most popular person of the batch. He fills places with energy and charm. 

Being part of a government engineering college gave us the opportunity to be free and think free. We were never spoon fed and always had to find our own ways to get things done. That, we think is the most important factor that a student need – Freedom to unleash his creativity. 


**Openode and Start of Entrepreneurship** 

We, along with couple of other friends started Openode Labs, an internet services startup while in the third year of engineering. The idea of having to do something had always been in our minds but we missed the right direction. We made money for our expenses and soon stopped getting money from home to cover our daily expenses. That was the first time we felt the ‘kick’ of doing something by own and being independent. We ran the company for 2 years and at the end of the undergraduate course, the team sat together to decide the future of Openode. The decision was to continue in the path but in a more serious way. The team parted ways to join different companies and decided they would come back within 3 years to get a strong headstart. 

**Corporate life**

The team went ahead and joined different companies – two with Infosys and two with startup companies. People around said that once we get addicted to one of the greatest addictions in the world – a monthly salary – we would never come back to start off on our own. At a corporate, what you get is the security and pleasure of sailing through a calm ocean while we were not made for that. We were surfers who love large waves and enjoyed riding on them. The passion to do something BIG brought us back to planning. 
The last year at the corporate was a time of rigorous planning and preparation during the non working hours. We worked 9 hours on our day jobs and spent the night on online calls to plan and align ourselves on this. Regular discussions formed and aligned the team and plans. 

**Startup Village**

The Startup Village experiment that was happening in Kerala and the wave of entrepreneurship that was starting in Kerala catalysed our decision to quit from our day jobs and be on the ground. As Sanjay says, in business, we are line surfers who ride on waves. You wait for the right wave and have the fun of riding on it. We did not want to miss the excitement of being part of the changing entrepreneurship eco system in Kerala. We resigned and joined Startup Village as the third incubated company here. 
From day one the journey has been one of excitement and being on the toes. Being a team that is never satisfied by their own work and strive to do better always, we have been enjoying one hell of a ride since start.
 
**The 7 months**

The seven months at Startup Village has been one of regular learning and adaptation. One fine day Sanjay walks into our room and talks to us for 30 minutes. BANG! We felt like we got 2 years worth of lessons on how business works in 30 minutes. Many more conversations, mentoring, experiences have made us to be one of the early success stories of Startup Village. We have, in a short span of 7 months, grabbed a handful of awards, gained traction and now, have made it into the Microsoft Accelerator. 
As a startup we are lucky and proud to be part of the changing startup ecosystem and the dawn of the silicon coast. In the startup world it is the community that matters more than anything else. With Startup Village and team Mobme, we do look forward to the day when the sun that sets in the silicon valley rises to witness the silicon coast here! 

Here we start Profoundis phase2! Stay tuned! #ACCELERATED
