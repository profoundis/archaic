Date: 2012-12-27
Title: Sentiment Analysis
Tags: Business Intelligence, Business Analytics, Sentiment Analysis
Slug: Understanding_Sentiments
Author: Jofin Joseph
Category: Emotion


*Business is all about Sentiments!* 

Like it or not, any business is all about sentiments! Your clients spread the sentiments about your company; so does your end consumers, employees, stakeholders and investors. The success of a business relies highly on understanding these sentiments and acting accordingly. 

Sentiments unveil large social patterns which can help you in consumer retention, consumer acquisition and future planning. 

But how do you get consumer/employee sentiments when you are running a large business? Yes it IS hectic. 

Sentiment analysis deals with collecting data from where people are conversing and intelligently analyse the sentiments hidden in these conversations. The data opens up awesome capabilities of customer complaint redressal, new lead generation and intelligent marketing. It powers a brand with the power of social data that is relevant to the enterprise. 

Further, social sentiment analysis can be a big part of market studies and market size estimations. *Yes people are talking out there! And now we have a means of understanding what they are talking!*
