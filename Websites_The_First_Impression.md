Date: 2012-09-17
Title: An Online File Sharing Website Discovered the Importance and Impact of Good Website Design and Analytics
Tags: Profoundis, Analytics, Business Optimization, Social Behaviour, Business Transactions, Reports
Slug: websites_the_first_impression
Author: Arjun R Pillai
Category: Analytics

**Abstract**

An Online sharing website was experiencing low traffic despite the company investing a good sum of 
money in marketing and online ads. 

**Challenge**

The reason for low traffic was unknown. The company was already investing a lot of money to 
marketing and hence couldn’t afford more investment in this respect. The Un-optimized page titles, 
tags, and copy were also causing problems for the popularity of website.
 
**Profoundis Solution**

<img alt="image alt" src="http://cdn.blog.profoundis.com/Web-Analytics-Diagram.jpg" width="75%">

Profoundis employed the Website Analytics on the existing site and 
figured that

 - Enough unique visitors were landing on the site (5000-6000) 
and many of them followed the marketing strategies. This 
means the marketing is pretty successful

 - The repeated visits were less (20-30% on an average).
 - Bounce rate was high (60 - 65% average)
 - Similarly signing up/logging in status were also disappointing

 

 
Profoundis figured that the problem was with the website. The site 
was not dynamic enough, not user friendly and was cluttered with lot of junk data and ads. The 
loading time of the site was also exceedingly high. 

Profoundis proposed and designed a new site keeping in mind the cleanliness, intuitiveness, response 
time and navigability. The new design focused on the interests and intuitions of the target customer 
group and  categorized the data  accordingly. This helped in cleaning up and arrangement of data, 
information and ads. This helped reduce the bounce rate to less than 25%. The tabs were shifted and 
made accessible from all pages in an intuitive way. A box for 
‘Trending files’ was added which featured the ‘Top shared’ files. 
FB/twitter integration increased the seamless movement of users
and their updates across the social networking platforms. The long 
registration was split into 2 stages, one at signing up and second 
one after being an active user of the site.

Advanced search engine optimization was done on the website. This 
increased the organic search traffic to the website. The complete 
website was relocated to the Profoundis servers with high response 
time and guaranteed uptime. Services Used:

 - Web Hosting Service
 - Web Site Design and Development
 - Search Engine Optimization

**Result**

The site showed steady increase in results:

 - Page views increased by 29%
 - Repeated customers were increased by 156%
 - Login rate increased by 197%
 - Search Engine Traffic increased by 162%
 - Bounce rate decreased by 170%
