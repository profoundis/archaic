Date: 2013-04-11
Title: Why I trust my friends more than a celebrity!
Tags: iTestify, social brand ambassador, testimonial
Slug: social-brand-ambassador
Author: Jofin Joseph
Category: iTestify

<img alt="image alt" src="http://cdn.blog.profoundis.com/social_ambassador.jpg" width="50%">
You are new to the town and wants to buy a new camera. You have two options one that the superstar in the films urges you to buy through the TV every single day and another the famous footballer tells you.  You ask your friend which to buy. He suggests a third one Which would you buy?

Well I would but the one that my friend suggests.

This has been the way the all the years and will remain the same. People trust people who walk around them and talk to them more than the shiny faces on the TV. But why is celebrity endorsements so popular and why are the brands spending more money paying the celebrities?

Well celebrity endoresements **were** the best way to reach out to the maximum number of people. Celebrities are of course the best bet while looking for a person that maximum number of people know. Or, there **were** no better way to find people closer to you than a celebrity to endorse a product to you!

Not Anymore!

That was the time before facebook, twitter and the internet as such. That was the time when an average person interacts with just 10 to 15 people a day. Now one tweet, one status update can reach thousands of people in my circle in a matter of minutes. The world is more connected to help you let know what you like and help you decide for you.

This is the time for **Social Brand Ambassadors**! 

http://itestifyit.com

 
