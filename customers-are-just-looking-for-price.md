Date: 2013-05-04
Title: Are customers just looking for Price?
Tags: iTestify, E-Commerce, online selling
Slug: customers-are-just-looking-for-price
Author: Jofin Joseph
Category: iTestify


The E-Commerce space of India with 6000+ companies, some selling everything under the sun to some reinventing the term ‘niche’ and limiting their inventory to two or even one item, is under a constant changes. The latest buzz word is the marketplace model, which the big players out there realizing as the only way to scale. There are lots of debate happening by the experts in the field if the E-Commerce model will sustain or burst or has already burst!

I am not getting into any of those debates since I am not an expert in the business of E-Commerce. But I am a consumer who have used these websites at least 500 times to buy my stuff. Yes, I am an ordinary Indian who compare the prices in different websites and many times go for the cheapest one. But is that all what affects the buying decisions? I do not think so!

It is the trust and security that matters the most in any transaction, online or offline. It is the trust that the Indian consumers did not have (still does not) which resulted in COD becoming so popular. That lack of trust results in 80% of the shipments being shipped as COD. Me being i the technology space always trusted these people and never ordered something on COD. It is the trust that is needed to attract the 90% of Indians who are still away from the E-Commerce to these online sellers.

Now, how do we build this trust? I am not talking about a magic want which can build the trust overnight. But the fact remains that people trust other people more than any goddamn software or machine. It is people and friends who can build this trust. Make the satisfies customers speak for you! Have social brand ambassadors all around to make this change. Then people will buy. People will pay money and buy.

Get your social stories out there! Change the mindset and make people pay and buy your stuff!

http://itestifyit.com
